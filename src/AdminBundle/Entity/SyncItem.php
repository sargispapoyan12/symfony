<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncItem
 *
 * @ORM\Table(name="sync_item")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SyncItemRepository")
 */
class SyncItem
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="syncBag", type="integer")
     */
    private $syncBag;

    /**
     * @var string
     *
     * @ORM\Column(name="grade", type="string", length=255)
     */
    private $grade;

    /**
     * @var string
     *
     * @ORM\Column(name="currentData", type="text")
     */
    private $currentData;

    /**
     * @var string
     *
     * @ORM\Column(name="historyData", type="text")
     */
    private $historyData;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set syncBag
     *
     * @param integer $syncBag
     * @return SyncItem
     */
    public function setSyncBag($syncBag)
    {
        $this->syncBag = $syncBag;

        return $this;
    }

    /**
     * Get syncBag
     *
     * @return integer 
     */
    public function getSyncBag()
    {
        return $this->syncBag;
    }

    /**
     * Set grade
     *
     * @param string $grade
     * @return SyncItem
     */
    public function setGrade($grade)
    {
        $this->grade = $grade;

        return $this;
    }

    /**
     * Get grade
     *
     * @return string 
     */
    public function getGrade()
    {
        return $this->grade;
    }

    /**
     * Set currentData
     *
     * @param string $currentData
     * @return SyncItem
     */
    public function setCurrentData($currentData)
    {
        $this->currentData = $currentData;

        return $this;
    }

    /**
     * Get currentData
     *
     * @return string 
     */
    public function getCurrentData()
    {
        return $this->currentData;
    }

    /**
     * Set historyData
     *
     * @param string $historyData
     * @return SyncItem
     */
    public function setHistoryData($historyData)
    {
        $this->historyData = $historyData;

        return $this;
    }

    /**
     * Get historyData
     *
     * @return string 
     */
    public function getHistoryData()
    {
        return $this->historyData;
    }
}
