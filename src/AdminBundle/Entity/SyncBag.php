<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncBag
 *
 * @ORM\Table(name="sync_bag")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SyncBagRepository")
 */
class SyncBag
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="source", type="integer")
     */
    private $source;

    /**
     * @var string
     *
     * @ORM\Column(name="syncItems", type="text")
     */
    private $syncItems;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set source
     *
     * @param integer $source
     * @return SyncBag
     */
    public function setSource($source)
    {
        $this->source = $source;

        return $this;
    }

    /**
     * Get source
     *
     * @return integer 
     */
    public function getSource()
    {
        return $this->source;
    }

    /**
     * Set syncItems
     *
     * @param string $syncItems
     * @return SyncBag
     */
    public function setSyncItems($syncItems)
    {
        $this->syncItems = $syncItems;

        return $this;
    }

    /**
     * Get syncItems
     *
     * @return string 
     */
    public function getSyncItems()
    {
        return $this->syncItems;
    }
}
