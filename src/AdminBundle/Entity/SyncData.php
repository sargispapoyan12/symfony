<?php

namespace AdminBundle\Entity;

use Doctrine\ORM\Mapping as ORM;

/**
 * SyncData
 *
 * @ORM\Table(name="sync_data")
 * @ORM\Entity(repositoryClass="AdminBundle\Repository\SyncDataRepository")
 */
class SyncData
{
    /**
     * @var int
     *
     * @ORM\Column(name="id", type="integer")
     * @ORM\Id
     * @ORM\GeneratedValue(strategy="AUTO")
     */
    private $id;

    /**
     * @var int
     *
     * @ORM\Column(name="status", type="integer")
     */
    private $status;

    /**
     * @var \DateTime
     *
     * @ORM\Column(name="created", type="datetimetz")
     */
    private $created;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString0", type="string", length=255)
     */
    private $dataString0;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString1", type="string", length=255)
     */
    private $dataString1;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString2", type="string", length=255)
     */
    private $dataString2;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString3", type="string", length=255)
     */
    private $dataString3;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString4", type="string", length=255)
     */
    private $dataString4;

    /**
     * @var string
     *
     * @ORM\Column(name="dataString5", type="string", length=255)
     */
    private $dataString5;


    /**
     * Get id
     *
     * @return integer 
     */
    public function getId()
    {
        return $this->id;
    }

    /**
     * Set status
     *
     * @param integer $status
     * @return SyncData
     */
    public function setStatus($status)
    {
        $this->status = $status;

        return $this;
    }

    /**
     * Get status
     *
     * @return integer 
     */
    public function getStatus()
    {
        return $this->status;
    }

    /**
     * Set created
     *
     * @param \DateTime $created
     * @return SyncData
     */
    public function setCreated($created)
    {
        $this->created = $created;

        return $this;
    }

    /**
     * Get created
     *
     * @return \DateTime 
     */
    public function getCreated()
    {
        return $this->created;
    }

    /**
     * Set dataString0
     *
     * @param string $dataString0
     * @return SyncData
     */
    public function setDataString0($dataString0)
    {
        $this->dataString0 = $dataString0;

        return $this;
    }

    /**
     * Get dataString0
     *
     * @return string 
     */
    public function getDataString0()
    {
        return $this->dataString0;
    }

    /**
     * Set dataString1
     *
     * @param string $dataString1
     * @return SyncData
     */
    public function setDataString1($dataString1)
    {
        $this->dataString1 = $dataString1;

        return $this;
    }

    /**
     * Get dataString1
     *
     * @return string 
     */
    public function getDataString1()
    {
        return $this->dataString1;
    }

    /**
     * Set dataString2
     *
     * @param string $dataString2
     * @return SyncData
     */
    public function setDataString2($dataString2)
    {
        $this->dataString2 = $dataString2;

        return $this;
    }

    /**
     * Get dataString2
     *
     * @return string 
     */
    public function getDataString2()
    {
        return $this->dataString2;
    }

    /**
     * Set dataString3
     *
     * @param string $dataString3
     * @return SyncData
     */
    public function setDataString3($dataString3)
    {
        $this->dataString3 = $dataString3;

        return $this;
    }

    /**
     * Get dataString3
     *
     * @return string 
     */
    public function getDataString3()
    {
        return $this->dataString3;
    }

    /**
     * Set dataString4
     *
     * @param string $dataString4
     * @return SyncData
     */
    public function setDataString4($dataString4)
    {
        $this->dataString4 = $dataString4;

        return $this;
    }

    /**
     * Get dataString4
     *
     * @return string 
     */
    public function getDataString4()
    {
        return $this->dataString4;
    }

    /**
     * Set dataString5
     *
     * @param string $dataString5
     * @return SyncData
     */
    public function setDataString5($dataString5)
    {
        $this->dataString5 = $dataString5;

        return $this;
    }

    /**
     * Get dataString5
     *
     * @return string 
     */
    public function getDataString5()
    {
        return $this->dataString5;
    }
}
